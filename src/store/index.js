import Vuex from 'vuex'
import Vue from 'vue'

//load Vuex
Vue.use(Vuex);

const state = {
    todoList: [
        {
            id: 1,
            title: "Complete ten Push-ups",
            dueDate: 'Dec 11',
            isCompleted: false 
        },
        {
            id: 2,
            title: "Interview Scheduled for Mike",
            dueDate: 'Dec 11',
            isCompleted: false 
        },
        {
            id: 3,
            title: "Meeting",
            dueDate: 'Dec 11',
            isCompleted: false 
        },
        {
            id: 4,
            title: "Party at Jason's house",
            dueDate: 'Dec 11',
            isCompleted: true 
        },
    ],
    taskInput: ''
}

const getters = {}

const actions = {
    
}

const mutations = {
    removeTask (state, taskId) {
        console.log(taskId)
        console.log(state.todoList)
        state.todoList = state.todoList.filter(val => val.id !== taskId)
        console.log(state.todoList)
    },
    addTask (state, task) {
        let duedate = new Date().toDateString().split(" ");
        let fullTaks={
            name: task,
            due: `${duedate[0]}${duedate[1]}${[2]}`
        }
        state.todoList.push(fullTaks);
    },
    updateTaskInput (state, taskInput) {
        state.taskInput = taskInput;
    },
}

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
})