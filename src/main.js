import Vue from 'vue'
import App from './App.vue'
import store from './store'
import './assets/styles/main.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrashAlt, faPlus, faTimes } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faTrashAlt, faCheckCircle, faPlus, faTimes)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
